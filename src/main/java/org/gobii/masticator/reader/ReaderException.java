package org.gobii.masticator.reader;

public class ReaderException extends RuntimeException {

	public ReaderException(String msg) {
		super(msg);
	}

}
