package org.gobii.masticator.aspects;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class RangeAspect extends ElementAspect {

	private int from;

}
