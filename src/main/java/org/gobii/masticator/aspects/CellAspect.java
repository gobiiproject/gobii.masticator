package org.gobii.masticator.aspects;

public class CellAspect extends CoordinateAspect {
	public CellAspect(Integer row, Integer col) {
		super(row, col);
	}
}
