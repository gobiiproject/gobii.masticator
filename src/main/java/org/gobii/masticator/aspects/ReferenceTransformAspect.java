package org.gobii.masticator.aspects;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReferenceTransformAspect extends TransformAspect {

	private String fname;

}
