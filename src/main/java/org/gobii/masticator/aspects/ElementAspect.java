package org.gobii.masticator.aspects;

import lombok.Data;

@Data
public class ElementAspect extends Aspect {

	private String name;

}
